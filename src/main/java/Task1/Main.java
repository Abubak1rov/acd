package Task1;

public class Main {
    public static void main(String[] args) {
        Node root = new Node(15);
        root.left = new Node(10);
        root.right = new Node(20);
        Node n1 = new Node(5);
        root.left.left = n1;
        root.left.right = new Node(13);
        Node n2 = new Node(14);
        root.left.right.right = n2;
        root.left.right.left = new Node(12);

        LowestCommonAncestorBST i = new LowestCommonAncestorBST();
        System.out.println("Recursive-Lowest Common Ancestor of Nodes "
                + n1.data + " and " + n2.data + " is : "
                + i.LCA(root, n1, n2));

    }

    private static class LowestCommonAncestorBST {
        public void LCA(Node root, Node n1, Node n2) {

        }
    }
}

