package week6;

public class Tree {
    private Node root;

    public Tree() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        Node current = root;
        if (current == null)
            root = new Node(key, value);
        while(current!=null) {
            if(key > current.getKey()){
                if(current.getRight() == null)
                    current.setRight(new Node(key, value));
            }
            else if(key < current.getKey()) {
                if (current.getLeft() == null)
                    current.setLeft(new Node(key, value));
            }
        }
        return;
    }



    public boolean delete(Integer key) {
        return false; //WRITE YOUR CODE HERE
    }

    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null)
            return null;
        else return node.getValue();
    }

    public Node findNode (Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey())
            return findNode(node.getRight(), key);
        else if(key < node.getKey())
            return findNode(node.getLeft(), key);
        else return node;
    }

    public void printAllAscending() {

    }

    public void printAll() {

    }

}
