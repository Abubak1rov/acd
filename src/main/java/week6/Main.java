package week6;

public class Main {
    public static void main(String[] args) {
        Tree bsTree = new Tree();

        bsTree.insert(100, "A");
        bsTree.insert(500, "B");
        bsTree.insert(200, "C");
        bsTree.insert(700, "D");
        bsTree.insert(600, "E");
        bsTree.insert(250, "F");
        bsTree.insert(800, "G");
        bsTree.insert(650, "H");
        bsTree.insert(808, "I");
        bsTree.insert(535, "Z");

        System.out.println("===Print 535 (Z)");
        System.out.println(bsTree.find(535));


    }

}
